package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.chubarov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public class Project {

    @Id
    @NotNull
    @Column(name = "id")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "pr_" + id.substring(1, 4);

    @Nullable
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Nullable
    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish = new Date();

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

}
