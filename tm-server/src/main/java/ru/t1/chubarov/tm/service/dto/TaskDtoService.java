package ru.t1.chubarov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.chubarov.tm.api.service.dto.ITaskDtoService;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chubarov.tm.exception.entity.TaskNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class TaskDtoService implements ITaskDtoService {

    @NotNull
    @Autowired
    public ITaskDtoRepository repository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @Nullable TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final String name,
            @NotNull final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull TaskDTO task = findOneById(userId, id);
        task.setStatus(status.toString());
        task.setUserId(userId);
        repository.saveAndFlush(task);
        return task;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.countAllByUserIdAndId(userId, id)>0;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = repository.findFirstById(id);
        if (model == null) throw new TaskNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();

        @Nullable final List<TaskDTO> tasks = repository.findAllByUserId(userId);
        if (tasks == null) throw new ModelNotFoundException();
        return tasks;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO remove(@NotNull final String userId, @Nullable final TaskDTO model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        repository.deleteById(model.getId());
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable TaskDTO model = new TaskDTO();
        repository.deleteByUserIdAndId(userId, id);
        return model;
    }

    @Override
    public long getSize() throws Exception {
        return repository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countAllByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<TaskDTO> add(@NotNull final Collection<TaskDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        for (@NotNull final TaskDTO task : models) {
            repository.saveAndFlush(task);
        }
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) throws Exception {
        if (models == null) throw new ProjectNotFoundException();
        repository.deleteAll();
        for (@NotNull final TaskDTO task : models) {
            repository.saveAndFlush(task);
        }
        return models;
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

}
