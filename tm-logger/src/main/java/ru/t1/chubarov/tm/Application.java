package ru.t1.chubarov.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chubarov.tm.component.Bootstrap;
import ru.t1.chubarov.tm.configuration.LoggerConfiguration;




public final class Application {

//    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
//
//    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.init();
//        @NotNull final LoggerService loggerService = new LoggerService();
//        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
//        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
//        @NotNull final Connection connection = connectionFactory.createConnection();
//        connection.start();
//        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
//        @NotNull final Queue destination = session.createQueue(QUEUE);
//        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
//        messageConsumer.setMessageListener(entityListener);
    }

}
