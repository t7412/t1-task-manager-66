package ru.t1.chubarov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.DataBackupLoadRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-load";
    @NotNull
    private final String DESCRIPTION = "Load backup data from file.";

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBackupLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpoint.loadDataBackup(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
