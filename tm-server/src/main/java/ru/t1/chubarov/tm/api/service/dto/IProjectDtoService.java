package ru.t1.chubarov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import java.util.Collection;
import java.util.List;


public interface IProjectDtoService {

    void create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    ProjectDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    ProjectDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    ProjectDTO changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    @NotNull
    ProjectDTO findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @NotNull
    ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    ProjectDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    long getSize() throws Exception;

    long getSize(@Nullable String userId) throws Exception;

    @NotNull
    Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models) throws Exception;

    @NotNull
    Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models) throws Exception;

    void clear();

}
