package ru.t1.chubarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskListRequest;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskShowListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskShowListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final TaskSort sort = TaskSort.toSort(sortType);

        @NotNull final TaskListRequest request = new TaskListRequest(sort, getToken());
        @NotNull final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        renderTask(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show list task.";
    }

}
