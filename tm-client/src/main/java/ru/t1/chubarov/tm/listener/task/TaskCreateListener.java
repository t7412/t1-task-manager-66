package ru.t1.chubarov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.dto.request.TaskCreateRequest;
import ru.t1.chubarov.tm.event.ConsoleEvent;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @Override
    @EventListener(condition = "@taskCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();

        @NotNull final TaskCreateRequest request = new TaskCreateRequest(name, description, getToken());
        request.setName(name);
        request.setDescription(description);
        taskEndpoint.createTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

}
