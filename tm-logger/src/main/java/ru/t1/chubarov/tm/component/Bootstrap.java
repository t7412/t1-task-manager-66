package ru.t1.chubarov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chubarov.tm.listener.EntityListener;
import ru.t1.chubarov.tm.service.ReceiverService;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private ReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(entityListener);
    }

}
