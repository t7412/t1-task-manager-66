package ru.t1.chubarov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chubarov.tm.api.ITaskRepository;
import ru.t1.chubarov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@Service
@NoArgsConstructor
public class TaskService {

    @NotNull
    @Autowired
    public ITaskRepository repository;

    @NotNull
    @Transactional
    public Task create(
            @Nullable final String name,
            @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EntityNotFoundException();
        if (description == null) throw new EntityNotFoundException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Transactional
    public Task add(@NotNull final Task task) {
        if (task.getName().isEmpty()) throw new EntityNotFoundException();
        repository.save(task);
        return repository.findById(task.getId()).orElse(null);
    }

    @Transactional
    public Task save(@NotNull final Task task) {
        if (task.getName().isEmpty()) throw new EntityNotFoundException();
        return repository.save(task);
    }

    public Collection<Task> findAll() {
        return repository.findAll();
    }

    public Task findById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return repository.findById(id).orElse(null);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        Task find_task = repository.findById(id).orElse(null);
        if (find_task != null) repository.deleteById(id);
    }

    @Transactional
    public void clear() {
        repository.deleteAll();
    }

}
