package ru.t1.chubarov.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;

public interface IProjectEndpoint {

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @GetMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

}
